#!/usr/bin/env bash
set -e

LOCAL_FILE=$1
LOCAL_FILE_NAME=$2

SSH_PORT=${SSH_PORT:=22}

EXIT_CODE=0

# check if env var exists
check_env_var() {
    env_var=$1
    env_var_name=$2
    if [ -z "$env_var" ]; then
      echo -e "[failed]"
      echo -e "--> ${env_var_name} not defined."
      echo -e ""
      EXIT_CODE=1
    fi
}

echo -ne "* Checking SSH ENV variables...\t"

# check env vars
check_env_var "$SSH_HOST" "SSH_HOST"
check_env_var "$SSH_USER" "SSH_USER"
check_env_var "$REMOTE_PATH" "REMOTE_PATH"

if [ "$EXIT_CODE" != 0 ]; then exit $EXIT_CODE; fi
if [ "$EXIT_CODE" == 0 ]; then echo -e "[OK]"; fi


if [ -z "$SSH_USER_PASSWORD" ]; then
    SSH_USER_PASSWORD=$SSH_USER
else
    SSH_USER_PASSWORD=$SSH_USER
fi


EXIT_CODE=0

push_file_with_rsync(){
    echo -ne "* Uploading via rsync...\t"
    sshpass -p "${SSH_PASSWORD}" rsync -aze "ssh -p $SSH_PORT -o StrictHostKeyChecking=no" $LOCAL_FILE $SSH_USER_PASSWORD@$SSH_HOST:$REMOTE_PATH/$LOCAL_FILE_NAME &> /dev/null
}

push_file_with_scp(){
    echo -ne "* Uploading via scp...\t"
    scp -P$SSH_PORT $LOCAL_FILE $SSH_USER_PASSWORD@$SSH_HOST:$REMOTE_PATH/$LOCAL_FILE_NAME
}

echo -ne "* Checking rsync existence ...\t"
if hash rsync 2>/dev/null; then
    echo -e "[OK]"
    push_file_with_rsync
    EXIT_CODE=$?
else
    if hash scp 2>/dev/null; then
        echo -e "[OK]"
        push_file_with_scp
        EXIT_CODE=$?
    else
        echo -e "[failed]"
        echo -e "--> rsync/scp not found."
        echo -e ""
        EXIT_CODE=1
    fi
fi

if [ "$EXIT_CODE" == 0 ]; then echo -e "[OK]"; fi
if [ "$EXIT_CODE" != 0 ]; then exit $EXIT_CODE; fi
