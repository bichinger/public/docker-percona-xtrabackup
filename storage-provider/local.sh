#!/usr/bin/env bash
set -e

EXIT_CODE=0
FILE_PATH=$1

# check if env var exists
check_env_var() {
    env_var=$1
    env_var_name=$2
    if [ -z "$env_var" ]; then
      echo -e "[failed]"
      echo -e "--> ${env_var_name} not defined."
      echo -e ""
      exit 1
    fi
}

echo -ne "* Checking Local ENV variables...\t\t"
check_env_var "$TARGET_DIR" "TARGET_DIR"
if [ "$EXIT_CODE" != 0 ]; then exit $EXIT_CODE; fi
if [ "$EXIT_CODE" == 0 ]; then echo -e "[OK]"; fi

# creates target dir to move file to
echo -ne "* Creating target dir...\t"
mkdir -p "${TARGET_DIR}"
return_code=$?
if [ $return_code -ne "0" ]; then
    echo -e "[failed]"
    echo "Creating target dir with exitcode ${return_code}"
else
    echo -e "[OK]"
fi

# move backup file
echo -ne "* Moving backupfile...\t\t"
mv "${FILE_PATH}" "${TARGET_DIR}"
return_code=$?
if [ $return_code -ne "0" ]; then
    echo -e "[failed]"
    echo "Move failed with exitcode ${return_code}"
else
    echo -e "[OK]"
fi