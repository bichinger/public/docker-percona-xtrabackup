#!/bin/bash
#set -e
## Test color support

EXIT_CODE=0
# start time tracking
begin=`date +%s`

CURRENT_DATE=$(date +%Y%m%d-%H%M%S)
LOG_FILE_NAME="${BACKUP_PROJECT_NAME}_${BACKUP_ENVIRONMENT}_${CURRENT_DATE}_innobackupex.txt"

# set filename and destination dirs
BACKUP_DIR="/tmp"
BACKUP_FILE_NAME="${BACKUP_PROJECT_NAME}_${BACKUP_ENVIRONMENT}_${CURRENT_DATE}_database.tar"
: ${MYSQL_USER:=root}
: ${MYSQL_PASSWORD:=$MYSQL_ROOT_PASSWORD}
: ${MYSQL_PORT:=3306}

# check if env var exists
check_env_var() {
    env_var=$1
    env_var_name=$2
    if [ -z "$env_var" ]; then
      echo -e "[failed]"
      echo -e "--> ${env_var_name} not defined."
      echo -e ""
      exit 1
    fi
}

# ping mysql to check when mysql is started
mysql_ready() {
    mysqladmin ping -h$MYSQL_HOST -uroot -p$MYSQL_PASSWORD >/dev/null 2>/dev/null
}


if [ -z "$BACKUP_DATABASES" ]; then
    BACKUP_DATABASES="--databases ${BACKUP_DATABASES}"
else
    BACKUP_DATABASES=""
fi


echo -e " Database Backup started...\t\t"
echo -ne "* Checking ENV variables...\t\t"
# check env vars
check_env_var "$BACKUP_STORAGE_PROVIDER" "BACKUP_STORAGE_PROVIDER"
check_env_var "$BACKUP_PROJECT_NAME" "BACKUP_PROJECT_NAME"
check_env_var "$BACKUP_ENVIRONMENT" "BACKUP_ENVIRONMENT"
check_env_var "$MYSQL_HOST" "MYSQL_HOST"
check_env_var "$MYSQL_PASSWORD" "MYSQL_PASSWORD"
check_env_var "$BACKUP_COMPRESSION" "BACKUP_COMPRESSION"
check_env_var "$BACKUP_ENCRYPTION_KEY" "BACKUP_ENCRYPTION_KEY"

if [ "$EXIT_CODE" != 0 ]; then exit $EXIT_CODE; fi
if [ "$EXIT_CODE" == 0 ]; then echo -e "[OK]"; fi


echo -ne "* Checking storage provider...\t\t"
# check if file exists for storagemode
if [ ! -f "./storage-provider/${BACKUP_STORAGE_PROVIDER}.sh" ]; then
    echo -e "[failed]"
    echo -e "--> Config file \"./storage-provider/${BACKUP_STORAGE_PROVIDER}.sh\" not found..."
    echo -e ""
    exit 1
fi
echo -e "[OK]"
echo -ne "* Checking mysql connection... \t\t"


## check mysql credentials
i=0
max_attempts=60
EXIT_CODE=0
while !(mysql_ready)
do
   sleep 1
   i=$[$i+1]
   if [ "$i" == "$max_attempts" ]; then
        echo -e "[failed]"
        echo -e "Connection retries exeeded after ${max_attempts} attempts. Aborting..."
        EXIT_CODE=1
        break
   fi
done

if [ "$EXIT_CODE" != 0 ]; then exit $EXIT_CODE; fi
if [ "$EXIT_CODE" == 0 ]; then echo -e "[OK]";fi


## creating backup file
echo -ne "* Creating backupfile... \t\t"
mkdir -p /tmp/logs/
nice -n 5 innobackupex --stream=tar /tmp $BACKUP_DATABASES --host=$MYSQL_HOST --port=$MYSQL_PORT --user=${MYSQL_USER:-root} --password=$MYSQL_PASSWORD > $BACKUP_DIR/$BACKUP_FILE_NAME 2> /tmp/logs/$LOG_FILE_NAME

return_code=$? #
# if failed, display errors and remove created files
if [ $return_code -ne "0" ]; then
    echo -e "[failed]"
    echo -e "Error while dumping (code $return_code), removing output file."
    echo -e ""
    echo -e "Content of logfile:"
    echo -e "------------------------------------------"
    echo -e ""
    tail -n 30 /tmp/logs/$LOG_FILE_NAME
    rm $BACKUP_DIR/$BACKUP_FILE_NAME
    echo -e ""
    echo -e "------------------------------------------"
    echo -e ""
    exit $return_code
fi
echo -e "[OK]"

## compressing output file
echo -ne "* Compressing with ${BACKUP_COMPRESSION}... \t\t"

# ... using xz
if [ "$BACKUP_COMPRESSION" == "xz" ]; then
	LOCAL_FILE_PATH=$BACKUP_DIR/$BACKUP_FILE_NAME.xz.gpg
	LOCAL_FILE_NAME=$BACKUP_FILE_NAME.xz.gpg
	nice -n 10 xz -q --best $BACKUP_DIR/$BACKUP_FILE_NAME -c | nice -n 10 gpg -q --symmetric --passphrase "$BACKUP_ENCRYPTION_KEY" --cipher-algo aes256 -o "$LOCAL_FILE_PATH" &> /dev/null
else
# ... using BZIP2
	if [ "$BACKUP_COMPRESSION" == "bzip2" ]; then
		LOCAL_FILE_PATH=$BACKUP_DIR/$BACKUP_FILE_NAME.bz2.gpg
	    LOCAL_FILE_NAME=$BACKUP_FILE_NAME.bz2.gpg
		nice -n 10 bzip2 -q --best $BACKUP_DIR/$BACKUP_FILE_NAME -c | nice -n 10 gpg -q --symmetric --passphrase "$BACKUP_ENCRYPTION_KEY" --cipher-algo aes256 -o "$LOCAL_FILE_PATH" &> /dev/null
	else
# ... using GZIP
        LOCAL_FILE_PATH=$BACKUP_DIR/$BACKUP_FILE_NAME.gz.gpg
	    LOCAL_FILE_NAME=$BACKUP_FILE_NAME.gz.gpg
        nice -n 10 gzip -q --best $BACKUP_DIR/$BACKUP_FILE_NAME -c | nice -n 10 gpg -q --symmetric --passphrase "$BACKUP_ENCRYPTION_KEY" --cipher-algo aes256 -o "$LOCAL_FILE_PATH" &> /dev/null
	fi
fi
return_code=$?
# if compression failed, remove created file
if [ $return_code -ne "0" ]; then
        echo -e "[failed]"
        echo -e "Error while compressing (code $return_code), removing compression output file."
        echo ""
        rm $LOCAL_FILE_PATH
        exit $return_code
fi
rm $BACKUP_DIR/$BACKUP_FILE_NAME
echo -e "[OK]"

# running storage provider
echo ""
echo -e "Storing backupfile with provider \"${BACKUP_STORAGE_PROVIDER}\":"
echo "-----------------------------"
echo ""
"./storage-provider/${BACKUP_STORAGE_PROVIDER}.sh" $LOCAL_FILE_PATH $LOCAL_FILE_NAME
return_code=$?
echo ""
echo "-----------------------------"
echo ""

if [ $return_code -ne "0" ]; then
    echo -e "[failed]"
    echo -e "Error while processing stroage provider \"${BACKUP_STORAGE_PROVIDER}\"."
    echo ""
    exit $return_code
fi

end=`date +%s`
execution_time=$(expr $end - $begin )
echo -e ">> Backup done in ${execution_time} sec."
echo ""