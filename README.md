# MySQL / Percona / MariaDB Backup Container

[![build status](https://gitlab.com/bichinger/docker-percona-xtrabackup/badges/master/build.svg)](https://gitlab.com/bichinger/docker-percona-xtrabackup/commits/master)

This is a one-shot container to (hot)backup database and move backupfile to destination of your choice.

Possible destinations (here called storage-provider) are: 
* local host filesystem
* rsync (ssh) to remote server
* Amazon S3
 
This container based on Percona XtraBackup script. See: https://www.percona.com/doc/percona-xtrabackup/2.4/index.html

**If something goes wrong:**
logfile of innobackupex-script located at /tmp/logs/innobackupex_output.txt on container and can be mounted to persist. Just add ````-v "<path-on-host>:/tmp/logs" ```` to view logfile



###### ------------------------------

## Possible ENV variables

#### MySQL related
* **MYSQL_HOST** MySQL Hostname
* **MYSQL_USER** MySQL Username
* **MYSQL_PASSWORD** MySQL Password
* **MYSQL_PORT** (optional) MySQL Port (default: 3306)

#### Backup related
* **BACKUP_PROJECT_NAME** Projectname. Appears at backupfilename
* **BACKUP_ENVIRONMENT** Project-Environment. Appears at backupfilename
* **BACKUP_ENCRYPTION_KEY** Encryptionkey for encryption of backup file
* **BACKUP_COMPRESSION** Compression used to compress backupfile. Possible values: **xz, bzip2 or gzip**
* **BACKUP_STORAGE_PROVIDER** (optional) Storage-Provider to process generated backupfile. Possible values: **local, s3 or ssh**
* **BACKUP_DATABASES** (optional) List of databases (default: all databases should back up if this option not given)

### StorageProvider related

#### Local
* **TARGET_DIR** Destination path for backupfile

##### Example Local command
    docker run \
    --link="`docker ps --filter name=<docker-db-service-name> --format "{{.ID}}"`" \
    --rm \
    -e TARGET_DIR="<container-target-dir>" \
    -e MYSQL_HOST="`docker ps --filter name=<docker-db-service-name> --format "{{.ID}}"`" \
    -e MYSQL_PASSWORD=<mysql-password> \
    -e MYSQL_USER=<mysql-user> \
    -e BACKUP_PROJECT_NAME=<my-projectname> \
    -e BACKUP_ENVIRONMENT=<environment> \
    -e BACKUP_ENCRYPTION_KEY=<encryption-key> \
    -e BACKUP_COMPRESSION=<compression-type> \
    -e BACKUP_STORAGE_PROVIDER=local \
    -v "<path-on-host>:<container-target-dir>" \
    --volumes-from="`docker ps --filter name=<docker-db-service-name> --format "{{.ID}}"`" \
    registry.gitlab.com/bichinger/docker-percona-backup:<version>
###### ------------------------------
#### SSH
* **SSH_HOST** Hostname for SSH connection
* **SSH_PORT** (optional) SSH Port (default: 22)
* **SSH_USER** Username for SSH connection
* **SSH_PASSWORD** Password for SSH connection
* **REMOTE_PATH** Target path on the remote server

##### Example SSH command
    docker run \
    --link="`docker ps --filter name=<docker-db-service-name> --format "{{.ID}}"`" \
    --rm \
    -e SSH_HOST="<ssh-host>" \
    -e SSH_PORT="<ssh-port>" \
    -e SSH_USER="<ssh-user>" \
    -e SSH_PASSWORD="<ssh-password>" \
    -e REMOTE_PATH="<remote-path>" \
    -e MYSQL_HOST="`docker ps --filter name=<docker-db-service-name> --format "{{.ID}}"`" \
    -e MYSQL_PASSWORD=<mysql-password> \
    -e MYSQL_USER=<mysql-user> \
    -e BACKUP_PROJECT_NAME=<my-projectname> \
    -e BACKUP_ENVIRONMENT=<environment> \
    -e BACKUP_ENCRYPTION_KEY=<encryption-key> \
    -e BACKUP_COMPRESSION=<compression-type> \
    -e BACKUP_STORAGE_PROVIDER=ssh \
    --volumes-from="`docker ps --filter name=<docker-db-service-name> --format "{{.ID}}"`" \
    registry.gitlab.com/bichinger/docker-percona-backup:<version>
###### ------------------------------
#### S3
* **S3_ACCESS_KEY_ID** Access key for S3 API
* **S3_BUCKET** Bucket Name to store into
* **S3_REGION** Region where the bucket is located
* **S3_SECRET_ACCESS_KEY** Secret access key for S3 API

##### Example S3 command
    docker run \
    --link="`docker ps --filter name=<docker-db-service-name> --format "{{.ID}}"`" \
    --rm \
    -e S3_ACCESS_KEY_ID="<S3-access-key-id>" \
    -e S3_BUCKET="<S3-bucket>" \
    -e S3_REGION="<S3-region>" \
    -e S3_SECRET_ACCESS_KEY="<S3-secret-access-key>" \
    -e MYSQL_HOST="`docker ps --filter name=<docker-db-service-name> --format "{{.ID}}"`" \
    -e MYSQL_PASSWORD=<mysql-password> \
    -e MYSQL_USER=<mysql-user> \
    -e BACKUP_PROJECT_NAME=<my-projectname> \
    -e BACKUP_ENVIRONMENT=<environment> \
    -e BACKUP_ENCRYPTION_KEY=<encryption-key> \
    -e BACKUP_COMPRESSION=<compression-type> \
    -e BACKUP_STORAGE_PROVIDER=s3 \
    --volumes-from="`docker ps --filter name=<docker-db-service-name> --format "{{.ID}}"`" \
    registry.gitlab.com/bichinger/docker-percona-backup:<version>

###### ------------------------------
### Example docker command for "local" storage provider

    docker run \
    --link="`docker ps --filter name=docker-db-service-name --format "{{.ID}}"`" \
    --rm \
    -e TARGET_DIR="/backups" \
    -e MYSQL_HOST="`docker ps --filter name=<docker-db-service-name> --format "{{.ID}}"`" \
    -e MYSQL_USER=root \
    -e MYSQL_PASSWORD=root \
    -e BACKUP_PROJECT_NAME=my-projectname \
    -e BACKUP_ENVIRONMENT=development \
    -e BACKUP_ENCRYPTION_KEY=zcFYmb6MR94Ddf8PdF31 \
    -e BACKUP_COMPRESSION=gzip \
    -e BACKUP_STORAGE_PROVIDER=local \
    -e BACKUP_DATABASES=my-database \
    -v "/my/local/path:/backups" \
    --volumes-from="`docker ps --filter name=docker-db-service-name --format "{{.ID}}"`" \
    registry.gitlab.com/bichinger/docker-percona-backup:<version>
###### ------------------------------
#### Command description per line:
###### docker run
###### --link="`docker ps --filter name=<docker-db-service-name> --format "{{.ID}}"`"
Make database-container available via local DNS. Determine database-container-ID by filtering for <docker-db-service-name>.
###### --rm
Remove container after exit
###### -e TARGET_DIR="/backups"
Specify path on backup-container to move backupfile to. This path needs to be mounted to local filesystem (on Docker Host) to access backupfile. Only available with "local" storage provider
###### -e MYSQL_HOST="`docker ps --filter name=<docker-db-service-name> --format "{{.ID}}"`"
Set MySQL-Host to container-id of the database-container. Every container can be reached by container-id via dns. Determine database-container-ID by filtering for <docker-db-service-name>
###### -e MYSQL_USER=root
MySQL username
###### -e MYSQL_PASSWORD=root
MySQL password
###### -e BACKUP_PROJECT_NAME=my-projectname
Projectname appears at backupfilename.
###### -e BACKUP_ENVIRONMENT=development
Project environment. Appears at backupfilename.
###### -e BACKUP_ENCRYPTION_KEY=zcFYmb6MR94Ddf8PdF31
Encryptionkey for encryption of backup file
###### -e BACKUP_COMPRESSION=gzip
Compression used to compress backupfile.
###### -e BACKUP_STORAGE_PROVIDER=local
Which storage provider to use
###### -e BACKUP_DATABASES=my-database
Database name to backup
###### -v "/my/local/path:/backups"
Mount host-folder to value of TARGET_DIR to access backup file on local filesystem. Only available with "local" storage provider
###### --volumes-from="`docker ps --filter name=docker-db-service-name --format "{{.ID}}"`"
Backupscript needs access to /var/lib/mysql on the database-container to acceess database-files.
###### bichinger/docker-percona-backup:1.1
Image name of backup-script-container
###### ------------------------------
## Backup Filename 
    ${BACKUP_PROJECT_NAME}_${BACKUP_ENVIRONMENT}_$(date +%Y%m%d-%H%M%S).tar.(xz|gz|bz2).gpg

###### ------------------------------

## Creating cronjob at cloud66

  1. In Stackoverview add a new "Server Task"-Add-In
  2. Select DockerHost from dropdown
  3. Specify Job Name
  4. Paste specified command (See above) into Command-Field.
  5. Specify Running frequency

###### ------------------------------
## Example command to backup cloud66 database server installed on host (no docker container)
Important: Install "Server-Task"-Add-In on Database-Host if it's not the same as the DockerHost

````
docker run \
--rm \
-e TARGET_DIR="<container-target-dir>" \
-e MYSQL_HOST="${MYSQL_ADDRESS}" \
-e MYSQL_USER="<mysql-admin-user>" \
-e MYSQL_PASSWORD="<mysql-admin-password>" \
-e BACKUP_PROJECT_NAME="<project-name>" \
-e BACKUP_ENVIRONMENT=production \
-e BACKUP_ENCRYPTION_KEY=<encryption-key> \
-e BACKUP_COMPRESSION=<compression-type> \
-e BACKUP_STORAGE_PROVIDER=local \
-e BACKUP_DATABASES="${MYSQL_DATABASE}" \
-v "<path-on-host>:<container-target-dir>" \
-v "/var/lib/mysql:/var/lib/mysql" \
registry.gitlab.com/bichinger/docker-percona-backup:<version>
````

###### ------------------------------

## How to Restore:
* un-tar (with ignore zeroes-option) to a tmp directory
* prepare the backup: innobackupex --apply-log /data/backups/2010-03-13_02-42-44/
* restore the backup: innobackupex --copy-back /data/backups/2010-03-13_02-42-44/
* check permissions: chown -R mysql:mysql /var/lib/mysql
* see: http://www.percona.com/doc/percona-xtrabackup/2.4/howtos/recipes_ibkx_local.html